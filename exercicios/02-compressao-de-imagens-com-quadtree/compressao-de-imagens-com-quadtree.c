/*
 *Aluno: Miguel Reis de Araújo
 *NºUSP: 12752457
 *Turma: SCC0201
 *
 *Nome do Trabalho:
 *   ____                                                          _      
 *  / ___|___  _ __ ___  _ __  _ __ ___  ___ ___  __ _  ___     __| | ___ 
 * | |   / _ \| '_ ` _ \| '_ \| '__/ _ \/ __/ __|/ _` |/ _ \   / _` |/ _ \
 * | |__| (_) | | | | | | |_) | | |  __/\__ \__ \ (_| | (_) | | (_| |  __/
 *  \____\___/|_| |_| |_| .__/|_|  \___||___/___/\__,_|\___/   \__,_|\___|
 *                      |_|                                               
 *  ___                                                            
 * |_ _|_ __ ___   __ _  __ _  ___ _ __  ___    ___ ___  _ __ ___  
 *  | || '_ ` _ \ / _` |/ _` |/ _ \ '_ \/ __|  / __/ _ \| '_ ` _ \ 
 *  | || | | | | | (_| | (_| |  __/ | | \__ \ | (_| (_) | | | | | |
 * |___|_| |_| |_|\__,_|\__, |\___|_| |_|___/  \___\___/|_| |_| |_|
 *                      |___/                                      
 *   ___                  _     _____              
 *  / _ \ _   _  __ _  __| |   |_   _| __ ___  ___ 
 * | | | | | | |/ _` |/ _` |_____| || '__/ _ \/ _ \
 * | |_| | |_| | (_| | (_| |_____| || | |  __/  __/
 *  \__\_\\__,_|\__,_|\__,_|     |_||_|  \___|\___|
 * 
 */

#include <stdio.h>
#include <stdlib.h>

int *quad_tree(int **image, int *tree, int *size, int ln_bgn, int ln_end,
			   int cl_bgn, int cl_end);

int main()
{
	int lines, columns;
	scanf(" %d %d", &lines, &columns);

	int **image = malloc(lines * sizeof(int *));

	for (int i = 0; i < lines; i++) {
		image[i] = malloc(columns * sizeof(int));
		for (int j = 0; j < columns; j++) {
			scanf(" %d", &image[i][j]);
		}
	}

	int *compressed_image = NULL;
	int size = 0;
	int ln_bgn = 0;
	int ln_end = lines;
	int cl_bgn = 0;
	int cl_end = columns;

	compressed_image = quad_tree(image, compressed_image, &size, ln_bgn, ln_end,
								 cl_bgn, cl_end);

	for (int i = 0; i < size; i++) {
		printf("%d ", compressed_image[i]);
	}
	putchar('\n');

	free(compressed_image);

	for (int i = 0; i < lines; i++) {
		free(image[i]);
	}

	free(image);

	return 0;
}


int *quad_tree(int **image, int *tree, int *size, int ln_bgn, int ln_end,
			   int cl_bgn, int cl_end)
{
	(*size)++;
	tree = realloc(tree, (*size) * sizeof(int));

	tree[(*size) - 1] = -1;
	
	int cl_mid = (cl_bgn + cl_end) / 2;
	int ln_mid = (ln_bgn + ln_end) / 2;

	// quadrante 1
	int node_value = image[ln_bgn][cl_bgn];

	for (int i = ln_bgn; i < ln_mid; i++) {
		for (int j = cl_bgn; j < cl_mid; j++) {
			if (image[i][j] != node_value) {
				node_value = -1;
				break;
			}
		}
	}

	if (node_value == -1) {
		tree = quad_tree(image, tree, size, ln_bgn, ln_mid, cl_bgn, cl_mid);
	} else {
		(*size)++;
		tree = realloc(tree, (*size) * sizeof(int));
		tree[(*size) - 1] = node_value;
	}

	// quadrante 2
	node_value = image[ln_bgn][cl_mid];

	for (int i = ln_bgn; i < ln_mid; i++) {
		for (int j = cl_mid; j < cl_end; j++) {
			if (image[i][j] != node_value) {
				node_value = -1;
				break;
			}
		}
	}

	if (node_value == -1) {
		tree = quad_tree(image, tree, size, ln_bgn, ln_mid, cl_mid, cl_end);
	} else {
		(*size)++;
		tree = realloc(tree, (*size) * sizeof(int));
		tree[(*size) - 1] = node_value;
	}

	// quadrante 3
	node_value = image[ln_mid][cl_bgn];

	for (int i = ln_mid; i < ln_end; i++) {
		for (int j = cl_bgn; j < cl_mid; j++) {
			if (image[i][j] != node_value) {
				node_value = -1;
				break;
			}
		}
	}

	if (node_value == -1) {
		tree = quad_tree(image, tree, size, ln_mid, ln_end, cl_bgn, cl_mid);
	} else {
		(*size)++;
		tree = realloc(tree, (*size) * sizeof(int));
		tree[(*size) - 1] = node_value;
	}

	// quadrante 4
	node_value = image[ln_mid][cl_mid];
	
	for (int i = ln_mid; i < ln_end; i++) {
		for (int j = cl_mid; j < cl_end; j++) {
			if (image[i][j] != node_value) {
				node_value = -1;
				break;
			}
		}
	}

	if (node_value == -1) {
		tree = quad_tree(image, tree, size, ln_mid, ln_end, cl_mid, cl_end);
	} else {
		(*size)++;
		tree = realloc(tree, (*size) * sizeof(int));
		tree[(*size) - 1] = node_value;
	}

	return tree;
}
