/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 */

#include <stdio.h>
#include <stdlib.h>

#define PESSOA '#'
#define CAMINHO '.'
#define VISITADO '*'

typedef struct {
	char **matriz;
	int n_linhas;
	int n_colunas;
} Labirinto;

typedef struct {
	int n_pessoas;
	int caminhos;
	int visitados;
	double exploracao;
} Informacoes;

char *read_line(FILE *stream);
int percorrer_labirinto(Labirinto *labirinto, int x, int y);
void imprimir_resultado(Labirinto *labirinto, Informacoes *info);
Informacoes ler_informacoes(Labirinto *labirinto);

int main()
{
	FILE *labirinto_arquivo;
	char *nome_arquivo = read_line(stdin);
	labirinto_arquivo = fopen(nome_arquivo, "r");

	Labirinto labirinto;
	fscanf(labirinto_arquivo, " %d", &labirinto.n_linhas);
	fscanf(labirinto_arquivo, " %d", &labirinto.n_colunas);

	int init_pos_y, init_pos_x;
	fscanf(labirinto_arquivo, " %d", &init_pos_y);
	fscanf(labirinto_arquivo, " %d", &init_pos_x);
	fscanf(labirinto_arquivo, " %*[\n]");

	labirinto.matriz = malloc(labirinto.n_linhas * sizeof(char *));
	for (int i = 0; i < labirinto.n_linhas; i++) {
		labirinto.matriz[i] = read_line(labirinto_arquivo);
	}

	fclose(labirinto_arquivo);
	free(nome_arquivo);

	percorrer_labirinto(&labirinto, init_pos_x, init_pos_y);

	Informacoes info = ler_informacoes(&labirinto);

	imprimir_resultado(&labirinto, &info);

	for (int i = 0; i < labirinto.n_linhas; i++)
		free(labirinto.matriz[i]);

	free(labirinto.matriz);

	return 0;
}


Informacoes ler_informacoes(Labirinto *labirinto)
{
	Informacoes info;
	info.n_pessoas = 0;
	info.caminhos = 0;
	info.visitados = 0;
	for (int i = 0; i < labirinto->n_linhas; i++) {
		for (int j = 0; j < labirinto->n_colunas; j++) {
			if (labirinto->matriz[i][j] == PESSOA) {
				info.n_pessoas++;
				continue;
			}

			if (labirinto->matriz[i][j] == CAMINHO) {
				info.caminhos++;
			} else if (labirinto->matriz[i][j] == VISITADO) {
				info.visitados++;
				info.caminhos++;
			}
		}
	}

	info.exploracao = (100 * info.visitados) / info.caminhos;

	return info;
}


void imprimir_resultado(Labirinto *labirinto, Informacoes *info)
{
	for (int i = 0; i < labirinto->n_linhas; i++) {
		for (int j = 0; j < labirinto->n_colunas; j++) {
			printf("%c", labirinto->matriz[i][j]);
		}
		printf("\n");
	}

	printf("\nVoce escapou de todos! Ninguem conseguiu te segurar!\n");
	printf("Veja abaixo os detalhes da sua fuga:\n");
	printf("----Pessoas te procurando: %d\n", info->n_pessoas);
	printf("----Numero total de caminhos validos:   %d\n", info->caminhos);
	printf("----Numero total de caminhos visitados: %d\n", info->visitados);
	printf("----Exploracao total do labirinto: %.1lf%%\n", info->exploracao);
}


int percorrer_labirinto(Labirinto *labirinto, int x, int y)
{
	if (labirinto->matriz[y][x] != CAMINHO)
		return 0;

	labirinto->matriz[y][x] = VISITADO;

	if (y <= 0 || y >= (labirinto->n_linhas - 1) || x <= 0 ||
		x >= (labirinto->n_colunas - 1))
		return 1;

	int final = 0;

	if (!final)
		final = percorrer_labirinto(labirinto, x, y - 1);

	if (!final)
		final = percorrer_labirinto(labirinto, x + 1, y);

	if (!final)
		final = percorrer_labirinto(labirinto, x, y + 1);

	if (!final)
		final = percorrer_labirinto(labirinto, x - 1, y);

	return final;
}


char *read_line(FILE *stream)
{
	int contador_caracteres = 0;
	int indice = 0;
	char *linha_lida = NULL;
	int stop = 0;

	do {
		indice = contador_caracteres;

		contador_caracteres += 1;

		linha_lida = realloc(linha_lida, contador_caracteres * sizeof(char));

		stop = fscanf(stream, "%c", &linha_lida[indice]) == EOF;

	} while (linha_lida[indice] != '\n' && linha_lida[indice] != '\r' && !stop);

	linha_lida[indice] = '\0';

	fscanf(stream, " %*[\n]");

	return linha_lida;
}
