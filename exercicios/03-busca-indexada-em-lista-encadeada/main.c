/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 * Curso: SCC0201
 *
 * Título Trabalho:
 *  ____                        ___           _                    _       
 * | __ ) _   _ ___  ___ __ _  |_ _|_ __   __| | _____  ____ _  __| | __ _ 
 * |  _ \| | | / __|/ __/ _` |  | || '_ \ / _` |/ _ \ \/ / _` |/ _` |/ _` |
 * | |_) | |_| \__ \ (_| (_| |  | || | | | (_| |  __/>  < (_| | (_| | (_| |
 * |____/ \__,_|___/\___\__,_| |___|_| |_|\__,_|\___/_/\_\__,_|\__,_|\__,_|
 *                                                                         
 *                   _     _     _        
 *   ___ _ __ ___   | |   (_)___| |_ __ _ 
 *  / _ \ '_ ` _ \  | |   | / __| __/ _` |
 * |  __/ | | | | | | |___| \__ \ || (_| |
 *  \___|_| |_| |_| |_____|_|___/\__\__,_|
 *                                        
 *  _____                     _                _       
 * | ____|_ __   ___ __ _  __| | ___  __ _  __| | __ _ 
 * |  _| | '_ \ / __/ _` |/ _` |/ _ \/ _` |/ _` |/ _` |
 * | |___| | | | (_| (_| | (_| |  __/ (_| | (_| | (_| |
 * |_____|_| |_|\___\__,_|\__,_|\___|\__,_|\__,_|\__,_|
 *                                                     
 */

#include <stdio.h>
#include <stdlib.h>
#include "string_plus.h"
#include <stdbool.h>
#include <string.h>

typedef struct node Node;

struct node {
	char *palavra;
	Node *next;
};

int inserir(Node *sentinela, char *palavra);
int update(Node *sentinela, Node **vetor);


int main()
{
	int comando;
	Node *sentinela = malloc(sizeof(Node));
	sentinela->palavra = NULL;
	sentinela->next = NULL;

	Node *vetor[26];

	for (int i = 0; i < 26; i++) {
		Node *novo_node = malloc(sizeof(Node));
		char *letra = malloc(sizeof(char));
		(*letra) = 'a' + i;

		novo_node->palavra = letra;
		novo_node->next = NULL;

		vetor[i] = novo_node;
	}

	bool updated = false;

	do {
		scanf(" %d", &comando);
		char aux = getchar();
		if (aux == '\r')
			getchar();

		if (comando == 1) {
			updated = false;

			char *nome_arquivo = NULL;
			read_word(&nome_arquivo, stdin);

			FILE *arquivo = fopen(nome_arquivo, "r");

			char *palavra = NULL;
			while (read_word(&palavra, arquivo) != EOF) {
				inserir(sentinela, palavra);
			}

			Node *aux = sentinela;

			for (int i = 0; i < 3; i++) {
				printf("%s\n", aux->next->palavra);
				aux = aux->next;
			}

			free(nome_arquivo);
			fclose(arquivo);

		} else if (comando == 2) {
			update(sentinela, vetor);
			updated = true;

			int contador = 0;
			for (int i = 0; i < 26; i++) {
				if (vetor[i]->next != NULL) {
					contador++;
				}
			}

			printf("%d\n", contador);

		} else if (comando == 3) {
			char *palavra = NULL;
			read_word(&palavra, stdin);

			if (updated) {
				Node *aux = vetor[palavra[0] - 'a'];
				int contador = 0;
				bool existe = false;

				while (aux->next != NULL &&
					   aux->next->palavra[0] == palavra[0]) {
					if (strcmp(aux->next->palavra, palavra) == 0) {
						existe = true;
						break;
					} else {
						aux = aux->next;
						contador++;
					}
				}

				if (existe) {
					printf("%d\n", contador);
				} else {
					printf("Palavra nao existe na lista.\n");
				}

			} else {
				printf("Vetor de indices nao atualizado.\n");
			}

			free(palavra);
		}

	} while (comando != 0);

	//free
	for (int i = 0; i < 26; i++) {
		free(vetor[i]->palavra);
		free(vetor[i]);
	}

	while (sentinela != NULL) {
		Node *aux = sentinela->next;

		if (sentinela->palavra != NULL)
			free(sentinela->palavra);

		free(sentinela);

		sentinela = aux;
	}

	return 0;
}


int inserir(Node *sentinela, char *palavra)
{
	Node *novo_node = malloc(sizeof(Node));
	novo_node->palavra = palavra;
	novo_node->next = NULL;

	if (sentinela->next == NULL) {
		sentinela->next = novo_node;
		return 0;
	}

	while (sentinela->next != NULL) {
		if (sentinela->next->palavra[0] >= palavra[0]) {
			novo_node->next = sentinela->next;
			sentinela->next = novo_node;
			return 0;
		} else {
			sentinela = sentinela->next;
		}
	}

	sentinela->next = novo_node;

	return 0;
}


int update(Node *sentinela, Node **vetor)
{
	char last_char = ' ';
	while (sentinela->next != NULL) {
		if (last_char != sentinela->next->palavra[0]) {
			last_char = sentinela->next->palavra[0];

			Node *aux = vetor[last_char - 'a'];
			aux->next = sentinela->next;
		}

		sentinela = sentinela->next;
	}

	return 0;
}
