#include "string_plus.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define true 1
#define false 0

typedef _Bool bool;


void replace(char *text, char *typo, char *fix)
{
	int text_size = strlen(text) < 101 ? strlen(text) : 101;
	int typo_size = strlen(typo);
	int fix_size = strlen(fix);
	char new_text[101];
	int index = 0;

	for (int i = 0; i < text_size; i++) {
		if (strncmp(&text[i], typo, typo_size) == 0) {
			for (int j = 0; j < fix_size; j++) {
				new_text[index] = fix[j];
				index++;
			}

			i += typo_size - 1;

			continue;
		}

		new_text[index] = text[i];
		index++;
	}

	for (int i = 0; i < index; i++) {
		text[i] = new_text[i];
	}

	if (text[index] != '\0')
		text[index] = '\0';
}


int read_line(char **line, FILE *stream)
{
	int contador_caracteres = 0;
	int indice = 0;
	char *linha_lida = NULL;
	bool stop = false;

	do {
		indice = contador_caracteres;

		contador_caracteres++;

		linha_lida = realloc(linha_lida, contador_caracteres * sizeof(char));

		stop = fscanf(stream, "%c", &linha_lida[indice]) == EOF;

	} while (!stop && linha_lida[indice] != '\n' && linha_lida[indice] != '\r');

	if (!stop && linha_lida[indice] == '\r') {
		getc(stream);
	}

	linha_lida[indice] = '\0';

	if (stop && contador_caracteres == 1) {
		free(linha_lida);
		return EOF;
	} else {
		(*line) = linha_lida;
		return 0;
	}
}


int read_word(char **word, FILE *stream)
{
	int contador_caracteres = 0;
	int indice = 0;
	char *palavra_lida = NULL;
	bool stop = false;

	do {
		indice = contador_caracteres;

		contador_caracteres++;

		palavra_lida =
			realloc(palavra_lida, contador_caracteres * sizeof(char));

		stop = fscanf(stream, "%c", &palavra_lida[indice]) == EOF;

	} while (!stop && palavra_lida[indice] != '\n' &&
			 palavra_lida[indice] != '\r' && palavra_lida[indice] != ' ');

	if (!stop && palavra_lida[indice] == '\r') {
		getc(stream);
	}

	palavra_lida[indice] = '\0';

	if (stop && contador_caracteres == 1) {
		free(palavra_lida);
		return EOF;
	} else {
		*word = palavra_lida;
		return 0;
	}
}
