/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 */

#include <stdio.h>
#include <stdlib.h>

#define MAX_NUM 100
#define COLUNAS 2

void reincidencia_numero(int numero, int *qt_numeros,
						 int vet_saida[MAX_NUM][COLUNAS]);


int main()
{
	int vetor_entrada[MAX_NUM];

	int indice = 0;
	while (!feof(stdin)) {
		scanf(" %d", &vetor_entrada[indice]);
		indice++;
	}

	int vetor_numeros_verificados[MAX_NUM][COLUNAS] = {};
	int qt_numeros = 0;
	for (int i = 0; i < indice; i++) {
		reincidencia_numero(vetor_entrada[i], &qt_numeros,
							vetor_numeros_verificados);
	}

	for (int i = 0; i < qt_numeros; i++) {
		printf("%d (%d)", vetor_numeros_verificados[i][0],
			   vetor_numeros_verificados[i][1]);

		if (i < (qt_numeros - 1))
			printf(" \n");
	}

	return 0;
}


void reincidencia_numero(int numero, int *qt_numeros,
						 int vet_saida[MAX_NUM][COLUNAS])
{
	for (int j = 0; j < (*qt_numeros); j++) {
		if (numero == vet_saida[j][0]) {
			vet_saida[j][1]++;
			return;
		}
	}

	vet_saida[*qt_numeros][0] = numero;
	vet_saida[*qt_numeros][1]++;
	(*qt_numeros)++;
}
