/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 * Curso: SCC0201
 *
 * Título Trabalho:
 *   ____    _    __  __ ____   ___    __  __ ___ _   _    _    ____   ___  
 *  / ___|  / \  |  \/  |  _ \ / _ \  |  \/  |_ _| \ | |  / \  |  _ \ / _ \ 
 * | |     / _ \ | |\/| | |_) | | | | | |\/| || ||  \| | / _ \ | | | | | | |
 * | |___ / ___ \| |  | |  __/| |_| | | |  | || || |\  |/ ___ \| |_| | |_| |
 *  \____/_/   \_\_|  |_|_|    \___/  |_|  |_|___|_| \_/_/   \_\____/ \___/ 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <assert.h>

#define IMPRIMIR 1
#define DICAS 2
#define USUARIO 3
#define VAZIO '.'
#define MINA '*'
#define OCULTO 'X'

typedef struct {
	int ln, col;
	char **matriz;
	char **matriz_usuario;
	int **dicas;
} Tabuleiro;

char *read_line(FILE *stream);
Tabuleiro ler_tabuleiro();
void imprimir(char **tabuleiro, int qt_linha);
void imprimir_dicas(Tabuleiro *tabuleiro);
void analizar_dicas(Tabuleiro *tabuleiro);
void preencher_matriz_usuario(Tabuleiro *tabuleiro);
void controle_usuario(Tabuleiro *tabuleiro);
void expandir(Tabuleiro *tabuleiro, int ln, int col);

int main()
{
	int opcao;
	scanf(" %d", &opcao);
	getchar();

	Tabuleiro tabuleiro = ler_tabuleiro();

	switch (opcao) {
	case IMPRIMIR:
		imprimir(tabuleiro.matriz, tabuleiro.ln);
		break;
	case DICAS:
		analizar_dicas(&tabuleiro);
		imprimir_dicas(&tabuleiro);
		break;
	case USUARIO:
		controle_usuario(&tabuleiro);
		break;
	}

	//Limpando memória
	for (int i = 0; i < tabuleiro.ln; i++) {
		free(tabuleiro.matriz[i]);
	}

	free(tabuleiro.matriz);

	return 0;
}


void controle_usuario(Tabuleiro *tabuleiro)
{
	int linha, coluna;
	scanf(" %d %d", &linha, &coluna);

	char pos = tabuleiro->matriz[linha][coluna];

	analizar_dicas(tabuleiro);

	if (pos == MINA) {
		imprimir_dicas(tabuleiro);
		return;
	}

	preencher_matriz_usuario(tabuleiro); // cria uma matriz preenchida com "X"

	if (tabuleiro->dicas[linha][coluna] > 0) {
		tabuleiro->matriz_usuario[linha][coluna] =
			tabuleiro->dicas[linha][coluna] + '0';

	} else {
		expandir(tabuleiro, linha, coluna);
	}

	imprimir(tabuleiro->matriz_usuario, tabuleiro->ln);

	//limpando memória
	for (int i = 0; i < tabuleiro->ln; i++) {
		free(tabuleiro->matriz_usuario[i]);
	}

	free(tabuleiro->matriz_usuario);

	for (int i = 0; i < tabuleiro->ln; i++) {
		free(tabuleiro->dicas[i]);
	}

	free(tabuleiro->dicas);
}


void expandir(Tabuleiro *tabuleiro, int linha, int coluna)
{
	if (tabuleiro->dicas[linha][coluna] > 0) {
		tabuleiro->matriz_usuario[linha][coluna] =
			tabuleiro->dicas[linha][coluna] + '0';
		return;
	} else {
		tabuleiro->matriz_usuario[linha][coluna] = VAZIO;
	}

	bool canto_direito = coluna >= (tabuleiro->col - 1);
	bool canto_inferior = linha >= (tabuleiro->ln - 1);
	bool canto_esquerdo = coluna <= 0;
	bool canto_superior = linha <= 0;

	if (!canto_direito &&
		tabuleiro->matriz_usuario[linha][coluna + 1] == OCULTO) {
		expandir(tabuleiro, linha, coluna + 1);
	}

	if (!canto_direito && !canto_inferior &&
		tabuleiro->matriz_usuario[linha + 1][coluna + 1] == OCULTO) {
		expandir(tabuleiro, linha + 1, coluna + 1);
	}

	if (!canto_inferior &&
		tabuleiro->matriz_usuario[linha + 1][coluna] == OCULTO) {
		expandir(tabuleiro, linha + 1, coluna);
	}

	if (!canto_inferior && !canto_esquerdo &&
		tabuleiro->matriz_usuario[linha + 1][coluna - 1] == OCULTO) {
		expandir(tabuleiro, linha + 1, coluna - 1);
	}

	if (!canto_esquerdo &&
		tabuleiro->matriz_usuario[linha][coluna - 1] == OCULTO) {
		expandir(tabuleiro, linha, coluna - 1);
	}

	if (!canto_esquerdo && !canto_superior &&
		tabuleiro->matriz_usuario[linha - 1][coluna - 1] == OCULTO) {
		expandir(tabuleiro, linha - 1, coluna - 1);
	}

	if (!canto_superior &&
		tabuleiro->matriz_usuario[linha - 1][coluna] == OCULTO) {
		expandir(tabuleiro, linha - 1, coluna);
	}

	if (!canto_superior && !canto_direito &&
		tabuleiro->matriz_usuario[linha - 1][coluna + 1] == OCULTO) {
		expandir(tabuleiro, linha - 1, coluna + 1);
	}
}


void preencher_matriz_usuario(Tabuleiro *tabuleiro)
{
	tabuleiro->matriz_usuario = malloc(tabuleiro->ln * sizeof(char *));

	for (int i = 0; i < tabuleiro->ln; i++) {
		tabuleiro->matriz_usuario[i] =
			malloc(tabuleiro->col + 1 * sizeof(char));
	}

	for (int i = 0; i < tabuleiro->ln; i++) {
		for (int j = 0; j < tabuleiro->col; j++) {
			tabuleiro->matriz_usuario[i][j] = 'X';
		}

		tabuleiro->matriz_usuario[i][tabuleiro->col] = '\0';
	}
}


void analizar_dicas(Tabuleiro *tabuleiro)
{
	tabuleiro->dicas = calloc(tabuleiro->ln, sizeof(int *));

	for (int i = 0; i < tabuleiro->ln; i++) {
		tabuleiro->dicas[i] = calloc(tabuleiro->col, sizeof(int));
	}

	for (int i = 0; i < tabuleiro->ln; i++) {
		for (int j = 0; j < tabuleiro->col; j++) {
			if (tabuleiro->matriz[i][j] == VAZIO) {
				/* resto do algoritimo será executado apenas se a posição atual
				 * for um bomba.
				 */
				continue;
			}

			bool canto_direito = j >= (tabuleiro->col - 1);
			bool canto_inferior = i >= (tabuleiro->ln - 1);
			bool canto_esquerdo = j <= 0;
			bool canto_superior = i <= 0;

			if (!canto_direito && tabuleiro->matriz[i][j + 1] == VAZIO) {
				tabuleiro->dicas[i][j + 1]++;
			}

			if (!canto_direito && !canto_inferior &&
				tabuleiro->matriz[i + 1][j + 1] == VAZIO) {
				tabuleiro->dicas[i + 1][j + 1]++;
			}

			if (!canto_inferior && tabuleiro->matriz[i + 1][j] == VAZIO) {
				tabuleiro->dicas[i + 1][j]++;
			}

			if (!canto_inferior && !canto_esquerdo &&
				tabuleiro->matriz[i + 1][j - 1] == VAZIO) {
				tabuleiro->dicas[i + 1][j - 1]++;
			}

			if (!canto_esquerdo && tabuleiro->matriz[i][j - 1] == VAZIO) {
				tabuleiro->dicas[i][j - 1]++;
			}

			if (!canto_esquerdo && !canto_superior &&
				tabuleiro->matriz[i - 1][j - 1] == VAZIO) {
				tabuleiro->dicas[i - 1][j - 1]++;
			}

			if (!canto_superior && tabuleiro->matriz[i - 1][j] == VAZIO) {
				tabuleiro->dicas[i - 1][j]++;
			}

			if (!canto_superior && !canto_direito &&
				tabuleiro->matriz[i - 1][j + 1] == VAZIO) {
				tabuleiro->dicas[i - 1][j + 1]++;
			}
		}
	}
}


void imprimir_dicas(Tabuleiro *tabuleiro)
{
	assert(tabuleiro->dicas != NULL);

	for (int i = 0; i < tabuleiro->ln; i++) {
		for (int j = 0; j < tabuleiro->col; j++) {
			if (tabuleiro->dicas[i][j] > 0)
				printf("%d", tabuleiro->dicas[i][j]);
			else
				printf("%c", tabuleiro->matriz[i][j]);
		}
		printf("\n");
	}

	//limpando memória
	for (int i = 0; i < tabuleiro->ln; i++) {
		free(tabuleiro->dicas[i]);
	}

	free(tabuleiro->dicas);
}


void imprimir(char **tabuleiro, int qt_linha)
{
	for (int i = 0; i < qt_linha; i++) {
		printf("%s\n", tabuleiro[i]);
	}
}


Tabuleiro ler_tabuleiro()
{
	char *nome_arquivo = read_line(stdin);

	FILE *arquivo = fopen(nome_arquivo, "r");

	free(nome_arquivo);

	Tabuleiro tabuleiro_temp;
	tabuleiro_temp.matriz = NULL;
	tabuleiro_temp.col = 0;
	tabuleiro_temp.ln = 0;

	while (!feof(arquivo)) {
		tabuleiro_temp.ln++;
		tabuleiro_temp.matriz =
			realloc(tabuleiro_temp.matriz, tabuleiro_temp.ln * sizeof(char *));

		tabuleiro_temp.matriz[tabuleiro_temp.ln - 1] = read_line(arquivo);
	}

	// o passo anterior lê uma linha a mais, então é preciso excluir ela
	tabuleiro_temp.ln--;
	free(tabuleiro_temp.matriz[tabuleiro_temp.ln]);
	tabuleiro_temp.matriz =
		realloc(tabuleiro_temp.matriz, tabuleiro_temp.ln * sizeof(char *));

	tabuleiro_temp.col = strlen(tabuleiro_temp.matriz[0]);

	fclose(arquivo);

	return tabuleiro_temp;
}


char *read_line(FILE *stream)
{
	int contador_caracteres = 0;
	int indice = 0;
	char *linha_lida = NULL;
	bool stop = false;

	do {
		indice = contador_caracteres;

		contador_caracteres += 1;

		linha_lida = realloc(linha_lida, contador_caracteres * sizeof(char));

		stop = fscanf(stream, "%c", &linha_lida[indice]) == EOF;

	} while (!stop && linha_lida[indice] != '\n' && linha_lida[indice] != '\r');

	linha_lida[indice] = '\0';

	return linha_lida;
}
