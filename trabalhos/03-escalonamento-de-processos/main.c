/*
 * Aluno: Miguel Reis de Araújo
 * NºUSP: 12752457
 * Curso: SCC0201
 *
 * Título Trabalho:
 *  ____  _                 _           _                  _      
 * / ___|(_)_ __ ___  _   _| | __ _  __| | ___  _ __    __| | ___ 
 * \___ \| | '_ ` _ \| | | | |/ _` |/ _` |/ _ \| '__|  / _` |/ _ \
 *  ___) | | | | | | | |_| | | (_| | (_| | (_) | |    | (_| |  __/
 * |____/|_|_| |_| |_|\__,_|_|\__,_|\__,_|\___/|_|     \__,_|\___|
 *                                                                
 *  _____               _                                        _        
 * | ____|___  ___ __ _| | ___  _ __   __ _ _ __ ___   ___ _ __ | |_ ___  
 * |  _| / __|/ __/ _` | |/ _ \| '_ \ / _` | '_ ` _ \ / _ \ '_ \| __/ _ \ 
 * | |___\__ \ (_| (_| | | (_) | | | | (_| | | | | | |  __/ | | | || (_) |
 * |_____|___/\___\__,_|_|\___/|_| |_|\__,_|_| |_| |_|\___|_| |_|\__\___/ 
 *                                                                        
 *      _        ____                                        
 *   __| | ___  |  _ \ _ __ ___   ___ ___  ___ ___  ___  ___ 
 *  / _` |/ _ \ | |_) | '__/ _ \ / __/ _ \/ __/ __|/ _ \/ __|
 * | (_| |  __/ |  __/| | | (_) | (_|  __/\__ \__ \ (_) \__	\
 *  \__,_|\___| |_|   |_|  \___/ \___\___||___/___/\___/|___/
 *                                                           
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAXINT

typedef struct node Node;

struct node {
	int id;
	int t_incial;
	int t_final;
	int prioridade;
	Node *next;
};

void destroy(Node *sentinela);
int get_process(Node *sentinela);
void round_robin(Node *sentinela, int process_counter);
int verify_new_process(Node *sentinela, Node **curr_process, int cycle_counter);


int main()
{
	Node *sentinela = malloc(sizeof(Node));
	sentinela->id = 0;
	sentinela->next = NULL;

	int process_counter = 0;

	while (get_process(sentinela) != EOF) {
		process_counter++;
	}

	round_robin(sentinela, process_counter);

	destroy(sentinela);

	return 0;
}


void round_robin(Node *sentinela, int process_counter)
{
	int cycle_counter = 0;
	Node *curr_process = NULL;

	while (process_counter > 0) {
		cycle_counter++;

		verify_new_process(sentinela, &curr_process, cycle_counter);

		if (curr_process->t_final <= 1) {
			curr_process->t_final--;
			process_counter--;
			printf("%d %d\n", curr_process->id, cycle_counter);
		} else {
			curr_process->t_final--;
		}
	}
}


int verify_new_process(Node *sentinela, Node **curr_process, int cycle_counter)
{
	Node *aux = sentinela;

	while ((*curr_process) == NULL) {
		if (aux->next->t_incial == cycle_counter) {
			(*curr_process) = aux->next;
			return 0;
		}

		aux = aux->next;
	}

	// se um processo de nível superior foi inciado
	while (aux != (*curr_process) && (*curr_process)->next != NULL) {
		if (aux->next->t_final > 0 && aux->next->t_incial == cycle_counter &&
			aux->next->prioridade > (*curr_process)->next->prioridade) {
			(*curr_process) = aux->next;
			return 0;
		}

		aux = aux->next;
	}

	// pegando próximo processo
	while (aux->next != (*curr_process)) {
		if (aux->next == NULL) {
			aux = sentinela;
		}

		if (aux->next->t_incial <= cycle_counter && aux->next->t_final > 0) {
			(*curr_process) = aux->next;
			return 0;
		}

		aux = aux->next;
	}

	return 0;
}


int get_process(Node *sentinela)
{
	int id;
	if (scanf(" %d", &id) == EOF) {
		return EOF;
	}

	int t_inicial, t_final, prioridade;
	scanf(" %d %d %d", &t_inicial, &t_final, &prioridade);

	Node *aux = sentinela;
	while (aux->next != NULL) {
		if (aux->next->id == id) {
			id++;
			break;
		}

		aux = aux->next;
	}

	Node *novo_processo = malloc(sizeof(Node));
	novo_processo->id = id;
	novo_processo->t_incial = t_inicial;
	novo_processo->t_final = t_final;
	novo_processo->prioridade = prioridade;
	novo_processo->next = NULL;

	aux = sentinela;
	while (aux->next != NULL &&
		   aux->next->prioridade >= novo_processo->prioridade) {
		if (aux->next->prioridade == novo_processo->prioridade &&
			aux->next->id > novo_processo->id) {
			break;
		}

		aux = aux->next;
	}

	novo_processo->next = aux->next;
	aux->next = novo_processo;

	return 0;
}


void destroy(Node *sentinela)
{
	Node *aux = sentinela;
	Node *to_destroy = NULL;

	while (aux->next != NULL) {
		to_destroy = aux;
		aux = aux->next;

		free(to_destroy);
	}

	free(aux);
}
