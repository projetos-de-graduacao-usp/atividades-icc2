/*
 *Aluno: Miguel Reis de Araújo
 *NºUSP: 12752457
 *Turma: SCC0201
 *
 *Nome do Trabalho:
 *   ____                                                          _
 *  / ___|___  _ __ ___  _ __  _ __ ___  ___ ___  __ _  ___     __| | ___
 * | |   / _ \| '_ ` _ \| '_ \| '__/ _ \/ __/ __|/ _` |/ _ \   / _` |/ _ \
 * | |__| (_) | | | | | | |_) | | |  __/\__ \__ \ (_| | (_) | | (_| |  __/
 *  \____\___/|_| |_| |_| .__/|_|  \___||___/___/\__,_|\___/   \__,_|\___|
 *                      |_|
 *     _             _ _
 *    / \  _   _  __| (_) ___
 *   / _ \| | | |/ _` | |/ _ \
 *  / ___ \ |_| | (_| | | (_) |
 * /_/   \_\__,_|\__,_|_|\___/
 *
 */

#include "data_processing.h"
#include <complex.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
	int index;
	double magnitude;
} Magnitude;

void merge_sort(Magnitude *magnitude_vec, int beg, int mid, int end);
void write_wav_data(char *fname, int lenght, unsigned char *data);
unsigned char *inv_DFT(double complex *coeff_vec, int length);
double magnitude(double complex value);


int main(int argc, char *argv[])
{
	char file_name[50];
	scanf(" %s", file_name);

	int n_coeff;
	scanf(" %d", &n_coeff);

	int audio_lenght;
	unsigned char *audio = read_wav_data(file_name, &audio_lenght);

	printf("%d\n", audio_lenght);

	double complex *coeff_vec = DFT(audio, audio_lenght);

	int n_less_or_equal_0 = 0;
	Magnitude *magnitude_vec = malloc(audio_lenght * sizeof(Magnitude));
	for (int i = 0; i < audio_lenght; i++) {
		magnitude_vec[i].index = i;
		magnitude_vec[i].magnitude = magnitude(coeff_vec[i]);

		if (creal(coeff_vec[i]) <= 0.0 && cimag(coeff_vec[i]) <= 0.0)
			n_less_or_equal_0++;
	}

	printf("%d\n", n_less_or_equal_0);

	int end = audio_lenght - 1;
	merge_sort(magnitude_vec, 0, (end / 2), end);

	for (int i = n_coeff; i < audio_lenght; i++) {
		coeff_vec[magnitude_vec[i].index] = 0.0;
	}

	for (int i = 0; i < n_coeff; i++) {
		printf("%d\n", (int)magnitude_vec[i].magnitude);
	}

	unsigned char *compressed_audio = inv_DFT(coeff_vec, audio_lenght);
	write_wav_data(file_name, audio_lenght, compressed_audio);

	free(compressed_audio);
	free(magnitude_vec);
	free(audio);
	free(coeff_vec);
	return 0;
}


void write_wav_data(char *fname, int lenght, unsigned char *data)
{
	char new_name[50] = "compressed_";
	strcat(new_name, fname);

	FILE *old_file = fopen(fname, "rb");

	FILE *new_file = fopen(new_name, "wb");

	unsigned char header[44];

	fread(header, sizeof(unsigned char), 44, old_file);

	fwrite(header, sizeof(unsigned char), 44, new_file);

	fwrite(data, sizeof(unsigned char), lenght, new_file);

	fclose(new_file);
	fclose(old_file);
}


unsigned char *inv_DFT(double complex *coeff_vec, int length)
{
	unsigned char *audio =
		(unsigned char *)calloc(length, sizeof(unsigned char));

	double complex temp_audio = 0.0;

	for (int n = 0; n < length; n++) {
		for (int k = 0; k < length; k++) {
			temp_audio +=
				coeff_vec[k] *
				cexp((2.0 * M_PI * (((k + 1) * n * 1.0) / (length * 1.0))) *
					 _Complex_I);
		}

		temp_audio *= (1.0 / (length * 1.0));

		audio[n] = (unsigned char)temp_audio;
	}

	return audio;
}


void merge_sort(Magnitude *magnitude_vec, int beg, int mid, int end)
{
	if (beg >= end) {
		return;
	}

	merge_sort(magnitude_vec, beg, ((beg + mid) / 2), mid);
	merge_sort(magnitude_vec, mid + 1, ((mid + 1) + end) / 2, end);

	Magnitude *sorted_vec = malloc(((end - beg) + 1) * sizeof(Magnitude));
	int sorted_index = 0;
	int half_1 = beg;
	int half_2 = mid + 1;

	// merge
	while (half_1 <= mid && half_2 <= end) {
		if (magnitude_vec[half_1].magnitude > magnitude_vec[half_2].magnitude) {
			sorted_vec[sorted_index] = magnitude_vec[half_1];
			sorted_index++;
			half_1++;

		} else {
			sorted_vec[sorted_index] = magnitude_vec[half_2];
			sorted_index++;
			half_2++;
		}
	}

	// dá merge no que sobrou em um dos vetores
	while (half_1 <= mid) {
		sorted_vec[sorted_index] = magnitude_vec[half_1];
		sorted_index++;
		half_1++;
	}

	while (half_2 <= end) {
		sorted_vec[sorted_index] = magnitude_vec[half_2];
		sorted_index++;
		half_2++;
	}

	// transfere os valores para o vetor original
	sorted_index = 0;
	for (int i = beg; i <= end; i++) {
		magnitude_vec[i] = sorted_vec[sorted_index];
		sorted_index++;
	}

	free(sorted_vec);
}


double magnitude(double complex value)
{
	return sqrt(pow(creal(value), 2) + pow(cimag(value), 2));
}
