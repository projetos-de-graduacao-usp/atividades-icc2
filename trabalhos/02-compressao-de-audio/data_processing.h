#ifndef _DATA_PROCESSING_H_
#define _DATA_PROCESSING_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <complex.h>
#include <math.h>

unsigned char *read_wav_data(char *fname, int *lenght);
/*
  Lê o qruivo fname e retorna seus dados na forma de um array 
  de unsigned char. Retorna o tamanho do array na variavel lenght.
*/

double complex *DFT(unsigned char *audio, int length);
// Discrete Fourier Transform

#endif /* _DATA_PROCESSING_H_ */
